<!--  Use this php code block to replace the php code block in CrimeMap.php to run local for tests. 
		Map.csv is a test data file.
		Make sure you have map.csv in the same folder as this file. 
		To run it on server, this php needs to be replaced by sqlToArray.php code. -->
		
<?php

$arrayCity = array();
$arrayCrime = array();

foreach(file('map.csv') AS $row) { //opens map.csv
    list($city, $crime) = explode(",", $row); 
    $arrayCity[] = $city; 
	$arrayCrime[] = $crime;
}

?>
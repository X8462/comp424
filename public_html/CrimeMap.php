<!--  
	  sqlToArray.php code block goes here to run it on server
	  -->
<?php
$con = mysqli_connect("mysql21.freehostia.com","shiag_cr","shikha1234","shiag_cr");
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }
 
	$query = "SELECT Category, Address FROM CrimeDetail";
	$result = mysqli_query($con, $query);

	$arrayCity = array();
	$arrayCrime = array();
	 
	if( $result ) {
		while($row = mysqli_fetch_array($result)) {
		$arrayCity[] = $row['Address'];
		$arrayCrime[] = $row['Category'];
		
	}
	//print_r($arrayCity);
	}
	 else {
		echo 'Select query failed';
	 }

 ?>

<!DOCTYPE html>
<html lang="en">

<head>
  	<title>Crime Mapping</title>
  	<meta charset="utf-8">
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Your name">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-1.7.1.min.js"></script>
    <script src="js/script.js"></script>

</head>
<body>

<div class="bg-main">
	<header>
		<div class="container">
			<div class="wrapper">
				<div class="div670px">
					<h1><a href="index.html">Idealex</a></h1>
				</div>
			</div>
		</div>
		<nav class="main-menu">
			<ul class="sf-menu">
				<li><a href="index.html"><em>Home</em><strong></strong></a></li>
				<li><a href="ReportCrime.html"><em>Report a Crime</em><strong></strong></a></li>
				<li class="current" ><a href="CrimeMap.php"><em>Crime Mapping</em><strong></strong></a></li>
				<li><a href="Newsroom.html"><em>Newsroom</em><strong></strong></a></li>
				<li><a href="MostWanted.html"><em>Most Wanted</em><strong></strong></a></li>
				<li><a href="AboutUs.html"><em>About Us</em><strong></strong></a></li>
				<li class="last"><a href="ContactUs.html"><em>Contact Us</em><strong></strong></a></li>
			</ul>


		<style>
			#map_canvas {
				width: 950px;
				height: 600px;
			}
			
			
			#tfheader{
				
		/* background-color:#c3dfef; */
	}
	#tfnewsearch{
		float:right;
		padding:20px;
	}
	
	/*tftextinput{
		margin: 0;
		padding: 5px 15px;
		font-family: Arial, Helvetica, sans-serif;
		font-size:14px;
		border:1px solid #0076a3; border-right:0px;
		border-top-left-radius: 5px 5px;
		border-bottom-left-radius: 5px 5px;
	}*/
	.tfbutton {
		margin: 0;
		padding: 5px 15px;
		font-family: Arial, Helvetica, sans-serif;
		font-size:14px;
		outline: none;
		cursor: pointer;
		text-align: center;
		text-decoration: none;
		color: #ffffff;
		border: solid 1px #0076a3; border-right:0px;
		background: #0095cd;
		background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
		background: -moz-linear-gradient(top,  #00adee,  #0078a5);
		border-top-right-radius: 5px 5px;
		border-bottom-right-radius: 5px 5px;
	}
	.tfbutton:hover {
		text-decoration: none;
		background: #007ead;
		background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
		background: -moz-linear-gradient(top,  #0095cc,  #00678e);
	}
	/* Fixes submit button height problem in Firefox */
	.tfbutton::-moz-focus-inner {
	  border: 0;
	}
	.tfclear{
		clear:both;
	}
			
		</style>

	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
	 
		<script>		
			var geocoder;
			var map;
			function initialize() {
				geocoder = new google.maps.Geocoder();
				var map_canvas = document.getElementById('map_canvas');
				var map_options = {
					center: new google.maps.LatLng(44.5403, -78.5463),
					zoom : 6,
					mapTypeId : google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map(map_canvas, map_options);				 
			}			
			
			function displayMap() {
				
				var places = <?php echo json_encode( $arrayCity ); ?>;
				var crimes = <?php echo json_encode( $arrayCrime ); ?>;	
				
				var addressCrimeMapping = [];
				
				for (var j = 0; j < places.length; j++){
					var address = places[j];
					var findCrime = crimes[j];
					
					var cleanCrime = findCrime.replace("\"", "").replace(",", ""); // Removes the apostrophe and comma at the beginning of the string
					var cleanAddress = address.replace("\"", ""); // Removes the apostrophe at the beginning of the string
					
					// Create key value pairs, where address is key and crime is value
					addressCrimeMapping[cleanAddress] = cleanCrime;
					
					//alert(addressCrimeMapping[cleanAddress]);
				};
				

				
				
					for (var i = 0; i < places.length; i++) {	
						var address = places[i];
						var findCrime = crimes[i];	
						
						var cleanAddress = address.replace("\"", "");
						geocoder.geocode({ 'address' : cleanAddress}, function(results, status) {
									//alert("Inside Geocode" + cleanAddress);
										if (status == google.maps.GeocoderStatus.OK) {
											map.setCenter(results[0].geometry.location);					
														
											var marker = new google.maps.Marker({  // <<-- Adds marker to map.
															map : map,
															position : results[0].geometry.location,
														});	
											
											var googleAddress = results[0].formatted_address;
														
										if (cleanAddress=googleAddress){
											var workingCrime = addressCrimeMapping[cleanAddress];
										}
											
										var crimeInformation = "\"" + workingCrime + "\"" + ", reported at " + " location: " + googleAddress;
																														
										// Adds crime information to marker
										var infowindow = new google.maps.InfoWindow();
										makeInfoWindowEvent(map, infowindow, crimeInformation, marker); 
											 
										} else {
											alert('Geocode was not successful for the following reason: ' + status);   // <<-- If the address is invalid
										}
										
								
						});
					}
			}
			
			
			// GEOCODE  (Entering address)
			function searchMap() {
				var address = document.getElementById('address').value;
				
				geocoder.geocode({
					'address' : address
				}  , function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						
						map.setCenter(results[0].geometry.location);
						
					} else {
						alert('Geocode was not successful for the following reason: ' + status);   // <<-- If the address one puts is invalid
					}
				});
			}

			// GEOCODE END

			
			function makeInfoWindowEvent(map, infowindow, contentString, marker) {
				
				google.maps.event.addListener(marker, 'click', function() {	
					infowindow.close;
					infowindow.setContent(contentString);
					infowindow.open(map, marker);
				});
			}
			
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
		
<body onload = "displayMap();">

		<form>
			
			<!-- HTML for SEARCH BAR -->
	<div id="tfheader">
		<form id="tfnewsearch" >
		        <input id="address" 
		        class="fleft" 
		        type="text" placeholder="Enter an address e.g. 400 W Lake, Chicago, IL"  
		        name="q" size="21" maxlength="120">
		        <input type="button" value="Search Map" class="tfbutton" onclick="searchMap()">
		</form>
	
	</div>
			
		</form>


		<!-- CRIME MAP -->
		<div id="map_canvas"></div>
	</div>	

<footer>
	<div class=" container">
		<div class="wrapper">
			<div class="div230px paddingRight80px">
				<h1 class="footer-logo"><a href="index.html">idealex</a></h1>
				<p>Welcome to the Crime Reporter</p>
				<p class="color000 privacy">&copy; 2013 <span>|</span>  <a href="AboutUs.html">Privacy Policy</a> <!-- {%FOOTER_LINK} --></p>
			</div>
			<div class="div270px paddingRight80px">
				<h4>Related Links</h4>
				<div class="wrapper">
					<div class="div150px alpha">
						<ul class="footer-list">
							<li><a href="index.html">Home</a></li>
						    <li><a href="ContactUs.html">Contact Us</a></li>
						</ul>
					</div>
					<div class="div110px omega">
						<ul class="footer-list">
							<li><a href="index.html">Sign Up</a></li>
							<li><a href="ReportCrime.html">Report a Crime</a></li>
							<li><a href="CrimeMap.php">Crime Mapping</a></li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="div270px">
				<h4>Follow Us</h4>
				<ul class="tooltips">
					<li><a href="Newsroom.html"><img src="images/icon1.png" alt=""><span>RSS</span></a></li>
					<li><a href="http://www.facebook.com"><img src="images/icon3.png" alt=""><span>Facebook</span></a></li>
					<li><a href="http://www.twitter.com"><img src="images/icon2.png" alt=""><span>Twitter</span></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
</body>
</html>